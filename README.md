# Stopwatch

simple stopwatch to use with polybar

to communicate with the stopwatch, the process exposes an
[xml/rpc](http://xmlrpc.com/) endpoint at `localhost:6969`. I'd prefer
to use a unix socket to serve an rpc connection, but
[s-xml-rpc](https://common-lisp.net/project/s-xml-rpc/) works out of
the box in common lisp, but only supports xml/rpc over http
connections so here I am.

The following methods are provided.

## `Stopwatch.Start` 

starts stopwatch if stopwatch has not already started, otherwise
errors. Provide a log to log the task in a sqlite database to track
how long `log` took.

### args

| arg   | type     | required | example            |
|-------|----------|----------|--------------------|
| `Log` | `string` | True     | `"lc decode ways"` |

### returns 

| return    | type     | example                    |
|-----------|----------|----------------------------|
| `Started` | `string` | `"started lc decode ways"` |

	
### example usage

```lisp
(ql:quickload "s-xml-rpc")
(s-xml-rpc:xml-rpc-call (s-xml-rpc:encode-xml-rpc-call "Stopwatch.Start" "lc decode ways") :host "localhost" :port 6969)
```

## `Stopwatch.Stop`

stops the currently running stopwatch, if one exists

### args

None

### returns 

| return    | type     | example                    |
|-----------|----------|----------------------------|
| `Stopped` | `string` | `"stopped lc decode ways"` |



### example usage
```lisp
(ql:quickload "s-xml-rpc")
(s-xml-rpc:xml-rpc-call (s-xml-rpc:encode-xml-rpc-call "Stopwatch.Stop" ) :host "localhost" :port 6969) 
```

## `Stopwatch.Restart`

restart the currently running stopwatch

### args

None

### returns 

| return      | type     | example                      |
|-------------|----------|------------------------------|
| `Restarted` | `string` | `"restarted lc decode ways"` |



### example usage
```lisp
(ql:quickload "s-xml-rpc")
(s-xml-rpc:xml-rpc-call (s-xml-rpc:encode-xml-rpc-call "Stopwatch.Restart" ) :host "localhost" :port 6969) 
```

## `Stopwatch.Cancel`

cancel the currently running stopwatch. Same as `Stopwatch.Stop` but prevents db entry from being entered.

### args

None

### returns 

| return      | type     | example                      |
|-------------|----------|------------------------------|
| `Cancelled` | `string` | `"cancelled lc decode ways"` |



### example usage
```lisp
(ql:quickload "s-xml-rpc")
(s-xml-rpc:xml-rpc-call (s-xml-rpc:encode-xml-rpc-call "Stopwatch.Cancel" ) :host "localhost" :port 6969) 
```

## `Stopwatch.Entries`

Get a list of current stopwatch entries for autocomplete

### args

None

### returns 

| return    | type           | example                            |
|-----------|----------------|------------------------------------|
| `Entries` | `list[string]` | `["lc decode ways", "lc two sum"]` |



### example usage
```lisp
(ql:quickload "s-xml-rpc")
(s-xml-rpc:xml-rpc-call (s-xml-rpc:encode-xml-rpc-call "Stopwatch.Entries" ) :host "localhost" :port 6969) 
```


install with 

```bash
go get gitlab.com/reedrichards/stopwatch
```

configure polybar

```ini
[module/stopwatch]
type = custom/script
exec = ~/go/bin/stopwatch 
label = %output%
tail = true
interval = 0
```


