package main

import (
	"fmt"
	"net/http"
	"time"

	"github.com/divan/gorilla-xmlrpc/xml"
	"github.com/gorilla/rpc"
)

// https://stackoverflow.com/questions/47341278/how-to-format-a-duration
func fmtDuration(d time.Duration) string {
	d = d.Round(time.Second)
	h := d / time.Hour
	d -= h * time.Hour
	m := d / time.Minute
	d -= m * time.Minute
	s := d / time.Second

	return fmt.Sprintf(
		"%02d:%02d:%02d", h, m, s,
	)

}

// StopWatchStation creates a stopwatch with channels that can signal
// the creation of or stoppage of the stopwatch.
type StopWatch struct {
	startTimer chan string
	endTimer   chan bool
	restart    chan bool
	cancel     chan bool
	log        *string
	db         *DB
}

func (stopwatch *StopWatch) run() {

	fmt.Printf("stopwatch: 00:00:00\n")

	done := make(chan bool, 1)
	cancel := make(chan bool, 1)
	restart := make(chan bool, 1)
	var duration time.Duration

	for {
		select {
		case item := <-stopwatch.startTimer:
			stopwatch.log = &item
			ticker := time.NewTicker(time.Second)
			start := time.Now()
			go func() {
				for {
					select {
					case <-done:
						stopwatch.db.insertEntry(stopwatch.log, duration.Milliseconds())
						durstr := fmtDuration(duration)
						fmt.Println(*stopwatch.log + ": " + durstr)
						stopwatch.log = nil
						ticker.Stop()
						return
					case <-cancel:
						durstr := fmtDuration(duration)
						fmt.Println(*stopwatch.log + ": " + durstr)
						stopwatch.log = nil
						ticker.Stop()
					case <-restart:
						ticker.Stop()
						ticker = time.NewTicker(time.Second)
						start = time.Now()
					case t := <-ticker.C:
						duration = t.Sub(start)
						durstr := fmtDuration(duration)
						fmt.Println(*stopwatch.log + ": " + durstr)
					}
				}
			}()

		case <-stopwatch.endTimer:
			done <- true
		case <-stopwatch.cancel:
			cancel <- true
		case <-stopwatch.restart:
			restart <- true

		}

	}

}

func (stopwatch *StopWatch) Start(r *http.Request, args *struct{ Log string }, reply *struct{ Started string }) error {
	stopwatch.startTimer <- args.Log
	reply.Started = "started " + args.Log
	return nil
}

func (stopwatch *StopWatch) Stop(r *http.Request, args *struct{}, reply *struct{ Stopped string }) error {
	reply.Stopped = "stopped " + *stopwatch.log
	stopwatch.endTimer <- true
	return nil
}

func (stopwatch *StopWatch) Restart(r *http.Request, args *struct{}, reply *struct{ Restarted string }) error {
	stopwatch.restart <- true
	reply.Restarted = "restarted " + *stopwatch.log
	return nil
}

func (stopwatch *StopWatch) Cancel(r *http.Request, args *struct{}, reply *struct{ Cancelled string }) error {
	stopwatch.cancel <- true
	reply.Cancelled = "cancelled " + *stopwatch.log
	return nil
}

func (stopwatch *StopWatch) Entries(r *http.Request, args *struct{}, reply *struct{ Entries []string }) error {
	reply.Entries = stopwatch.db.uniqueEntries()
	return nil
}

func main() {
	db := &DB{
		location: "/home/rob/.stopwatch.db", // TODO path module
	}
	db.init()
	RPC := rpc.NewServer()
	xmlrpcCodec := xml.NewCodec()
	RPC.RegisterCodec(xmlrpcCodec, "text/xml")
	service := &StopWatch{
		startTimer: make(chan string, 1),
		endTimer:   make(chan bool, 1),
		restart:    make(chan bool, 1),
		cancel:     make(chan bool, 1),
		db:         db,
	}
	go service.run()
	RPC.RegisterService(service, "")
	http.Handle("/", RPC)

	// log.Println("Starting XML-RPC server on localhost:6969/RPC2")
	http.ListenAndServe(":6969", nil)
}
