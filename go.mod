module gitlab.com/reedrichards/stopwatch

go 1.14

require (
	github.com/divan/gorilla-xmlrpc v0.0.0-20190926132722-f0686da74fda
	github.com/gorilla/rpc v1.2.0
	github.com/mattn/go-sqlite3 v1.14.6
	github.com/rogpeppe/go-charset v0.0.0-20190617161244-0dc95cdf6f31 // indirect
)
