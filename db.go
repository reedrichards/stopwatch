package main

import (
	"database/sql"
	"log"

	_ "github.com/mattn/go-sqlite3"
)

type DB struct {
	location string
}

func (db *DB) init() {

	d, err := sql.Open("sqlite3", db.location)
	if err != nil {
		log.Fatal(err)
	}
	defer d.Close()

	sqlStatement := `
CREATE TABLE IF NOT EXISTS entry (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  log TEXT,
  duration INT
);
`

	_, err = d.Exec(sqlStatement)
	if err != nil {
		log.Printf("%q: %s\n", err, sqlStatement)
		return
	}
}

func (db *DB) insertEntry(item *string, duration int64) {
	d, err := sql.Open("sqlite3", db.location)
	if err != nil {
		log.Fatal(err)
	}
	defer d.Close()

	tx, err := d.Begin()
	if err != nil {
		log.Fatal(err)
	}
	stmt, err := tx.Prepare("INSERT INTO entry(log, duration) values(?, ?);")
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()
	_, err = stmt.Exec(item, duration)
	if err != nil {
		log.Fatal(err)
	}

	tx.Commit()
}

func (db *DB) uniqueEntries() []string {
	d, err := sql.Open("sqlite3", db.location)
	if err != nil {
		log.Fatal(err)
	}
	defer d.Close()

	rows, err := d.Query(`
SELECT DISTINCT log from entry
WHERE log IS NOT NULL AND log IS NOT "";
`)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	var entries []string
	for rows.Next() {
		var entry string
		err = rows.Scan(&entry)
		if err != nil {
			log.Fatal(err)
		}
		entries = append(entries, entry)

	}
	return entries
}
